# Templates

JTL-Shop nutzt die Template-Engine [Smarty](https://www.smarty.net/). Im Frontend kommen HTML5,
[Bootstrap](https://getbootstrap.com/) sowie [jQuery](https://jquery.com/) und diverse jQuery-Plugins zum Einsatz.

Es stehen verschiedene Möglichkeiten für individuelle Templateanpassungen zur Verfügung, auf die in dieser
Dokumentation näher eingegangen werden soll.

Zur zusätzlichen Unterstützung bei Templateanpassungen existiert auch das
JTL-Plugin [JTL Debug](../shop_programming_tips/debug.md).

* [JTL-Shop-Templates](jtl_templates.md)  
  Allgemeine Informationen über die aktuellen JTL-Shop-Templates

* [Struktur](structure.md)  
Informationen zur Ordner- und Dateistruktur des Templates, sowie zum Aufbau der ``template.xml``

* [Eigenes Template](eigenes_template.md)  
Child-Templates, "Der bevorzugte Weg zum eigenen Template"

* [Template Einstellungen](template_settings.md)  
Die einzelnen Einstellungen des Templates genauer erklärt

* [Eigenes Theme](eigenes_theme.md)  
Eigene individuelle Designs erstellen, per Stylesheets (CSS, LESS und SCSS)

* [Themes editieren](theme_edit.md)  
Einfache Anpassungen von Farben und Abstände mittels Theme-Anpassungen |br|
Hier werden die verschiedenen Methoden erklärt.

* [Artikelsticker](product_badges.md)  
Artikelsticker (wie z.B. "Bestseller") selbst erstellen

* [Tipps und Tricks](tipps_tricks.md)  
Template How-To's und Best Practices
