# HOOK_ARTIKEL_INC_METADESCRIPTION (59)

## Triggerpunkt

Vor der Rückgabe der meta-descrption des Artikels, in den Artikeldetails

## Parameter

* `string` **&cDesc** - die meta-description des Artikels
* `JTL\Catalog\Product\Artikel` **&oArtikel** - das entsprechende Artikel-Objekt