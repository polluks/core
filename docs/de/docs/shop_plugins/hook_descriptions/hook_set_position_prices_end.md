# HOOK_SET_POSITION_PRICES_END (413)

## Triggerpunkt

Am Ende vom Setzen der Positionspreise einer jeden Position

## Parameter

* `JTL\Cart\CartItem` **position** - neues Artikelobjekt
* `JTL\Cart\CartItem` **oldPosition** - altes Artikelobjekt
