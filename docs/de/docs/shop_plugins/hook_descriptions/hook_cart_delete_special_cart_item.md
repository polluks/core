# HOOK_CART_DELETE_SPECIAL_CART_ITEM (418)

## Triggerpunkt

Vor Entfernen von Spezial-Positionen aus dem Warenkorb.

## Parameter

* `JTL\Cart\CartItem` **positionItem** - Die zu löschende Position
* `bool` **delete** - Boolean Flag, ob die Position gelöscht werden soll
