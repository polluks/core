# HOOK_FILTER_INC_GIBARTIKELKEYS (178)

## Triggerpunkt

Am Ende von `gibArtikelKeys()`

## Parameter

* `array` **&oArtikelKey_arr** - Zusammenstellung der Artikel-IDs
* `stdClass` **FilterSQL** - Abfragefilter
* `JTL\Filter\ProductFilter` **NaviFilter** - Artikelfilter-Objekt
* `stdClass` **&SortierungsSQL** - Objekt mit Sortierungsinformationen