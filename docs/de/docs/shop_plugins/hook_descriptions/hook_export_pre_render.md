# HOOK_EXPORT_PRE_RENDER (350)

## Triggerpunkt

Vor dem Rendern eines einzelnen Artikels während eines Exportdurchgangs

## Parameter

* `JTL\Export\Product` **product** - Instanz des aktuell exportierten Produkts
* `JTL\Export\FormatExporter` **exporter** - Instanz des aktiven Exporters
* `int` **exportID** - ID des aktiven Exportformats