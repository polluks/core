# HOOK_REVIEWMANAGER_ADDREWARD_END (416)

## Triggerpunkt

Vor Absender der Bewertungsguthaben-Email.

## Parameter

* `JTL\Review\ReviewModel` **review** - Das geladene Model der Bewertung
* `JTL\Review\ReviewBonusModel` **reviewBonus** - Das geladene Model des Bewertungsguthabens
* `float` **reward** - Referenz auf das Guthaben
