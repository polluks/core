<?php

declare(strict_types=1);

namespace Tests\Helpers;

use DateTime;
use JTL\Helpers\Date;
use PHPUnit\Framework\TestCase;

class DateTest extends TestCase
{
    public function testGetMonthNam(): void
    {
        $this->assertSame('January', Date::getMonthName(1));
        $this->assertSame('September', Date::getMonthName('9'));
        $this->assertSame('September', Date::getMonthName(21));
    }

    public function testGetLastDayOfMonth(): void
    {
        $this->assertIsInt(Date::getLastDayOfMonth());
        $this->assertIsInt(Date::getLastDayOfMonth(12));
        $ts = Date::getLastDayOfMonth(12, 2024);
        $this->assertIsInt($ts);
        $dt = new DateTime();
        $dt->setTimestamp($ts);
        $this->assertSame('31', $dt->format('d'));
    }

    public function testGetFirstDayOfMonth(): void
    {
        $this->assertIsInt(Date::getFirstDayOfMonth());
        $this->assertIsInt(Date::getFirstDayOfMonth(12));
        $ts = Date::getFirstDayOfMonth(12, 2024);
        $this->assertIsInt($ts);
        $dt = new DateTime();
        $dt->setTimestamp($ts);
        $this->assertSame('01', $dt->format('d'));
    }

    public function testGetWeekStartAndEnd(): void
    {
        $this->assertSame([], Date::getWeekStartAndEnd(''));
        $this->assertSame([1724623200, 1725227999], Date::getWeekStartAndEnd('2024-08-28'));
    }

    public function testConvertDateToMysqlStandard(): void
    {
        $this->assertSame('_DBNULL_', Date::convertDateToMysqlStandard(null));
        $this->assertSame('_DBNULL_', Date::convertDateToMysqlStandard(''));
        $this->assertSame('2024-08-28', Date::convertDateToMysqlStandard('2024-08-28'));
        $this->assertSame('2024-08-28', Date::convertDateToMysqlStandard('28.08.2024'));
        $this->assertSame('2024-04-03', Date::convertDateToMysqlStandard('3.4.2024'));
    }

    public function testLocalize(): void
    {
        $this->assertSame('28.08.2024', Date::localize('2024-08-28', true));
        $this->assertSame('01.08.2024', Date::localize('2024-8-1', true));
        $this->assertSame('03.04.2024 00:00', Date::localize('2024-04-03'));
        $this->assertSame('03.04.2024 00:00', Date::localize('2024-4-3'));
    }

    public function testGetDateParts(): void
    {
        $this->assertSame([], Date::getDateParts(''));
        $this->assertSame([], Date::getDateParts('invaliddatestring'));
        $this->assertCount(8, Date::getDateParts('now'));
        $res = Date::getDateParts('2024-08-28 12:34:56');
        $this->assertSame('2024-08-28', $res['cDatum']);
        $this->assertSame('12:34:56', $res['cZeit']);
        $this->assertSame('2024', $res['cJahr']);
        $this->assertSame('08', $res['cMonat']);
        $this->assertSame('28', $res['cTag']);
        $this->assertSame('12', $res['cStunde']);
        $this->assertSame('34', $res['cMinute']);
        $this->assertSame('56', $res['cSekunde']);
    }

    public function testDateAddWeekday(): void
    {
        $expectedDT = new DateTime('2024-09-05');
        $srcDT      = new DateTime('2024-08-28');
        $srcTS      = $srcDT->getTimestamp();
        $this->assertEquals($expectedDT, Date::dateAddWeekday('2024-08-28', 8));
        $this->assertEquals($expectedDT, Date::dateAddWeekday($srcDT, 8));
        $this->assertEquals($expectedDT, Date::dateAddWeekday($srcTS, 8));
        $this->assertEquals(
            (new DateTime('2024-09-02'))->format('d'),
            Date::dateAddWeekday('2024-09-02', -1)->format('d')
        );
    }
}
