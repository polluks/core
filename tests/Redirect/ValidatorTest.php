<?php

declare(strict_types=1);

namespace Tests\Redirect;

use JTL\DB\NiceDB;
use JTL\Redirect\Repositories\RedirectRepository;
use JTL\Redirect\Services\RedirectService;
use JTL\Redirect\Services\ValidationService;
use Tests\UnitTestCase;

class ValidatorTest extends UnitTestCase
{
    private ValidationService $validator;

    private RedirectRepository $repository;

    public function setUp(): void
    {
        $this->repository = $this->getMockBuilder(RedirectRepository::class)
            ->enableOriginalConstructor()
            ->setConstructorArgs([$this->createStub(NiceDB::class)])
            ->onlyMethods(['getItemBySource'])
            ->getMock();
        $service          = new RedirectService($this->repository);
        $this->validator  = new ValidationService($service);
    }

    public function dtestTest(): void
    {
        $this->assertFalse($this->validator->test(''));
        $this->assertFalse($this->validator->test('/'));
        $this->assertFalse($this->validator->test('/test.jpg'));
        $this->repository->method('getItemBySource')
            ->willReturn(
                (object)[
                    'cToUrl'        => '/destination',
                    'cFromUrl'      => '/source',
                    'nCount'        => '23',
                    'kRedirect'     => '1',
                    'cAvailable'    => 'n',
                    'type'          => '1',
                    'paramHandling' => '0',
                    'dateCreated'   => '2024-08-08 13:30:22',
                ]
            );
        $this->assertSame('/destination', $this->validator->test('/test'));
    }

    public function ytestTestWithQueryString(): void
    {
        $this->repository->method('getItemBySource')
            ->willReturn(
                (object)[
                    'cToUrl'        => '/destination',
                    'cFromUrl'      => '/source?x=y',
                    'nCount'        => '23',
                    'kRedirect'     => '1',
                    'cAvailable'    => 'n',
                    'type'          => '1',
                    'paramHandling' => '0',
                    'dateCreated'   => '2024-08-08 13:30:22',
                ]
            );
        $this->assertSame('/destination', $this->validator->test('foo?x=y'));
    }

    public function testTestWithQueryStringFound(): void
    {
        $this->repository->method('getItemBySource')
            ->willReturnOnConsecutiveCalls(
                null,
                (object)[
                    'cToUrl'        => '/destination',
                    'cFromUrl'      => '/source?x=y',
                    'nCount'        => '23',
                    'kRedirect'     => '1',
                    'cAvailable'    => 'n',
                    'type'          => '1',
                    'paramHandling' => '0',
                    'dateCreated'   => '2024-08-08 13:30:22',
                ]
            );
        $this->assertSame('/destination?x=y', $this->validator->test('foo?x=y'));
    }

    public function testIsValid(): void
    {
        $this->assertTrue($this->validator->isValid('test'));
        $this->assertTrue($this->validator->isValid('/test'));
        $this->assertTrue($this->validator->isValid('/test/'));
        $this->assertTrue($this->validator->isValid('/test/test.exe'));
        $this->assertTrue($this->validator->isValid('/test.exe'));
        $this->assertTrue($this->validator->isValid('test.exe'));

        $this->assertFalse($this->validator->isValid('test.jpg'));
        $this->assertFalse($this->validator->isValid('test.JPG'));
        $this->assertFalse($this->validator->isValid('test.jpeg'));
        $this->assertFalse($this->validator->isValid('test.gif'));
        $this->assertFalse($this->validator->isValid('TEST.GIF'));
        $this->assertFalse($this->validator->isValid('test.bmp'));
        $this->assertFalse($this->validator->isValid('test.bMP'));
        $this->assertFalse($this->validator->isValid('test.xml'));
        $this->assertFalse($this->validator->isValid('test.XML'));
        $this->assertFalse($this->validator->isValid('test.ico'));
        $this->assertFalse($this->validator->isValid('test.ICO'));
        $this->assertFalse($this->validator->isValid('test.txt'));
        $this->assertFalse($this->validator->isValid('test.TXt'));
        $this->assertFalse($this->validator->isValid('test.png'));
        $this->assertFalse($this->validator->isValid('test.pNG'));
        $this->assertFalse($this->validator->isValid('/test/test.gif'));
        $this->assertFalse($this->validator->isValid('/test/test.xml/'));
        $this->assertFalse($this->validator->isValid('/test/test.txt/'));
        $this->assertFalse($this->validator->isValid('http://example.com/test.xml'));
        $this->assertFalse($this->validator->isValid('http://example.com/test/test.txt/'));
    }
}
