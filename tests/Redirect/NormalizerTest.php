<?php

declare(strict_types=1);

namespace Tests\Redirect;

use JTL\Redirect\Helpers\Normalizer;
use Tests\UnitTestCase;

class NormalizerTest extends UnitTestCase
{
    private Normalizer $normalizer;

    public function setUp(): void
    {
        $this->normalizer = new Normalizer();
    }

    public function testNormalize(): void
    {
        $this->assertEquals('/', $this->normalizer->normalize('/'));
        $this->assertEquals('/', $this->normalizer->normalize(''));
        $this->assertEquals('/test', $this->normalizer->normalize('/test'));
        $this->assertEquals('/test', $this->normalizer->normalize('test'));
        $this->assertEquals('/test', $this->normalizer->normalize('test/'));
        $this->assertEquals('/test', $this->normalizer->normalize('/test', false));
        $this->assertEquals('/test/', $this->normalizer->normalize('test/', false));
        $this->assertEquals('/test/', $this->normalizer->normalize('/test/', false));
        $this->assertEquals('/test/', $this->normalizer->normalize('/test/', false));
        $this->assertEquals('/', $this->normalizer->normalize('////test/'));
        $this->assertEquals('/a/b', $this->normalizer->normalize('/a/b/////'));
        $this->assertEquals('/a/b/////', $this->normalizer->normalize('/a/b/////', false));
        $this->assertEquals('http://example.com', $this->normalizer->normalize('http://example.com'));
        $this->assertEquals('http://example.com', $this->normalizer->normalize('http://example.com/'));
        $this->assertEquals('http://example.com/', $this->normalizer->normalize('http://example.com/', false));
    }
}
