<?php

declare(strict_types=1);

namespace Tests\Catalog;

use InvalidArgumentException;
use JTL\Catalog\Separator;
use JTL\DB\DbInterface;
use JTL\DB\NiceDB;
use JTL\Language\LanguageModel;
use JTL\Shop;
use stdClass;
use Tests\UnitTestCase;

class SeparatorTest extends UnitTestCase
{
    private DbInterface $db;

    public function setUp(): void
    {
        $this->db = $this->createStub(NiceDB::class);
    }

    private function getExampleSeparator(): Separator
    {
        $separator = new Separator(0, $this->db);
        $separator->setTrennzeichen(99);
        $separator->setSprache(1);
        $separator->setEinheit(\JTL_SEPARATOR_WEIGHT);
        $separator->setDezimalstellen(4);
        $separator->setTausenderZeichen('T');
        $separator->setDezimalZeichen('D');

        return $separator;
    }

    private function getExampleObject(): stdClass
    {
        $data                    = new stdClass();
        $data->kTrennzeichen     = 99;
        $data->kSprache          = 1;
        $data->nEinheit          = \JTL_SEPARATOR_WEIGHT;
        $data->nDezimalstellen   = 4;
        $data->cTausenderZeichen = 'T';
        $data->cDezimalZeichen   = 'D';

        return $data;
    }

    public function testSave(): void
    {
        $item = $this->getExampleSeparator();
        $this->db->method('insert')->willReturnOnConsecutiveCalls(0, 8, 42);
        $this->assertFalse($item->save());
        $this->assertTrue($item->save(false));
        $this->assertEquals(42, $item->save());
    }

    public function testDelete(): void
    {
        $this->db->method('delete')->willReturn(1);
        $item = new Separator(99, $this->db);
        $this->assertEquals(1, $item->delete());
    }

    public function testUpdate(): void
    {
        $item = $this->getExampleSeparator();
        $this->db->method('update')->willReturn(1);
        $this->assertEquals(1, $item->update());
    }

    public function testNewInstance(): void
    {
        $obj = $this->getExampleObject();
        $this->db->method('select')->willReturn($obj);
        $item = new Separator(99, $this->db);

        $this->assertEquals($obj->kTrennzeichen, $item->getTrennzeichen());
        $this->assertEquals($obj->kSprache, $item->getSprache());
        $this->assertEquals($obj->nEinheit, $item->getEinheit());
        $this->assertEquals($obj->nDezimalstellen, $item->getDezimalstellen());
        $this->assertEquals($obj->cTausenderZeichen, $item->getTausenderZeichen());
        $this->assertEquals($obj->cDezimalZeichen, $item->getDezimalZeichen());
    }

    public function testGetteretter(): void
    {
        $item = new Separator(0, $this->db);
        $obj  = $this->getExampleObject();
        $item->setTrennzeichen($obj->kTrennzeichen);
        $item->setSprache($obj->kSprache);
        $item->setEinheit($obj->nEinheit);
        $item->setDezimalstellen($obj->nDezimalstellen);
        $item->setTausenderZeichen($obj->cTausenderZeichen);
        $item->setDezimalZeichen($obj->cDezimalZeichen);

        $this->assertEquals($obj->kTrennzeichen, $item->getTrennzeichen());
        $this->assertEquals($obj->kSprache, $item->getSprache());
        $this->assertEquals($obj->nEinheit, $item->getEinheit());
        $this->assertEquals($obj->nDezimalstellen, $item->getDezimalstellen());
        $this->assertEquals($obj->cTausenderZeichen, $item->getTausenderZeichen());
        $this->assertEquals($obj->cDezimalZeichen, $item->getDezimalZeichen());
    }

    public function testGetUnit(): void
    {
        $obj = $this->getExampleObject();
        $this->db->method('select')->willReturn($obj);
        Shop::Container()->setSingleton(DbInterface::class, fn() => $this->db);
        $this->assertEquals('123T456T789D0123', Separator::getUnit(1, 1, 123456789.0123456));
        $this->assertEquals('1T234D5600', Separator::getUnit(1, 1, 1234.56));
        $this->assertEquals('0D0000', Separator::getUnit(1, 1, 0));
        $this->assertEquals('-12D0000', Separator::getUnit(1, 1, -12));
        $this->assertEquals('-12D0000', Separator::getUnit(1, 1, '-12'));
        $this->expectException(InvalidArgumentException::class);
        Separator::getUnit(1024, 12, 44);
    }

    public function testInsert(): void
    {
        $obj                    = $this->getExampleObject();
        $obj->nDezimalstellen   = 2;
        $obj->cDezimalZeichen   = ',';
        $obj->cTausenderZeichen = '.';
        $lo                     = new LanguageModel($this->db);
        $lo->setId(1);
        $lo->setIso('ger');
        $lo->setShopDefault('y');
        $_SESSION['Sprachen']    = [$lo];
        $_SESSION['cISOSprache'] = 'ger';
        $_SESSION['kSprache']    = 1;
        $this->db->method('getCollection')->willReturn(\collect());
        $this->db->method('select')->willReturnOnConsecutiveCalls(null, $obj);
        $this->db->method('insert')->willReturn(13);
        Shop::Container()->setSingleton(DbInterface::class, fn() => $this->db);
        $this->assertEquals('4,56', Separator::getUnit(13, 1, 4.56));
    }
}
