<?php

declare(strict_types=1);

namespace Tests\Checkout;

use Monolog\JsonSerializableDateTimeImmutable;
use Psr\Log\LoggerInterface;

/**
 * Class MockLogger
 * @package Tests\Checkout
 */
class MockLogger implements LoggerInterface
{
    public const DEBUG = 100;

    public const INFO = 200;

    public const NOTICE = 250;

    public const WARNING = 300;

    public const ERROR = 400;

    public const CRITICAL = 500;

    public const ALERT = 550;

    public const EMERGENCY = 600;

    private string $lastMessage = '';

    public function addRecord(
        int $level,
        string $message,
        array $context = [],
        ?JsonSerializableDateTimeImmutable $datetime = null
    ): bool {
        $this->lastMessage = $message;

        return true;
    }

    public function getLastMessage(): string
    {
        return $this->lastMessage;
    }

    public function reset(): void
    {
        $this->lastMessage = '';
    }

    /**
     * @inheritdoc
     */
    public function emergency(string|\Stringable $message, array $context = []): void
    {
        $this->addRecord(100, (string)$message);
    }

    /**
     * @inheritdoc
     */
    public function alert(string|\Stringable $message, array $context = []): void
    {
        $this->addRecord(100, (string)$message);
    }

    /**
     * @inheritdoc
     */
    public function critical(string|\Stringable $message, array $context = []): void
    {
        $this->addRecord(100, (string)$message);
    }

    /**
     * @inheritdoc
     */
    public function error(string|\Stringable $message, array $context = []): void
    {
        $this->addRecord(100, (string)$message);
    }

    /**
     * @inheritdoc
     */
    public function warning(string|\Stringable $message, array $context = []): void
    {
        $this->addRecord(100, (string)$message);
    }

    /**
     * @inheritdoc
     */
    public function notice(string|\Stringable $message, array $context = []): void
    {
        $this->addRecord(100, (string)$message);
    }

    /**
     * @inheritdoc
     */
    public function info(string|\Stringable $message, array $context = []): void
    {
        $this->addRecord(100, (string)$message);
    }

    /**
     * @inheritdoc
     */
    public function debug(string|\Stringable $message, array $context = []): void
    {
        $this->addRecord(100, (string)$message);
    }

    /**
     * @inheritdoc
     */
    public function log($level, string|\Stringable $message, array $context = []): void
    {
        $this->addRecord(100, (string)$message);
    }
}
